package com.ami.books

import android.content.Context
import android.support.test.InstrumentationRegistry
import android.support.test.espresso.Espresso
import android.support.test.espresso.Espresso.onView
import android.support.test.espresso.action.ViewActions.click
import android.support.test.espresso.action.ViewActions.typeText
import android.support.test.espresso.assertion.ViewAssertions.matches
import android.support.test.espresso.contrib.RecyclerViewActions
import android.support.test.espresso.matcher.ViewMatchers.*
import android.support.test.rule.ActivityTestRule
import android.support.test.runner.AndroidJUnit4
import android.widget.ProgressBar
import com.ami.books.ui.BooksAdapter
import org.junit.Assert.assertEquals
import org.junit.Before
import org.junit.Rule
import org.junit.Test
import org.junit.runner.RunWith

/**
 * Instrumentation test, which will execute on an Android device.
 *
 * @see [Testing documentation](http://d.android.com/tools/testing)
 */
@RunWith(AndroidJUnit4::class)
class UiTest {

    lateinit var appContext: Context
    @Rule @JvmField
    val activity = ActivityTestRule<MainActivity>(MainActivity::class.java)
    lateinit var idlingResource: ProgressIdlingResource

    @Before
    fun setUp() {
        appContext = InstrumentationRegistry.getTargetContext()
        assertEquals("com.ami.books", appContext.packageName)
        idlingResource = ProgressIdlingResource(activity.activity.findViewById(R.id.progressBar) as ProgressBar)
        Espresso.registerIdlingResources(idlingResource)
    }

    @Test
    fun testSearch() {
        //When
        onView(withId(R.id.editTextSearch)).perform(typeText("Hello world"))

        //Then
        onView(withId(R.id.recyclerViewBooks)).check(matches(isDisplayed()))
        onView(withId(R.id.recyclerViewBooks)).perform(RecyclerViewActions.actionOnItem<BooksAdapter.BookViewHolder>(
                hasDescendant(withText("Hello world")),
                click()))
    }

}

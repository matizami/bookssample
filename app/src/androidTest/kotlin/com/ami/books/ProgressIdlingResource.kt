package com.ami.books

import android.support.test.espresso.IdlingResource
import android.view.View
import android.widget.ProgressBar

class ProgressIdlingResource(private val progressView: ProgressBar): IdlingResource {

    private var idlingResourceCallback: IdlingResource.ResourceCallback? = null
    private var isIdle = true

    override fun getName(): String {
        return progressView.id.toString()
    }

    override fun isIdleNow(): Boolean {
        val idle = progressView.visibility == View.GONE
        if (!isIdle && idle && idlingResourceCallback != null) {
            idlingResourceCallback!!.onTransitionToIdle()
        }
        this.isIdle = idle
        return idle
    }

    override fun registerIdleTransitionCallback(resourceCallback: IdlingResource.ResourceCallback?) {
        idlingResourceCallback = resourceCallback
    }

}


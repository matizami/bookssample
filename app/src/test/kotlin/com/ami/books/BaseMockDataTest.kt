package com.ami.books

import com.ami.books.network.model.VolumesResponse
import com.google.gson.GsonBuilder
import io.reactivex.android.plugins.RxAndroidPlugins
import io.reactivex.plugins.RxJavaPlugins
import io.reactivex.schedulers.Schedulers
import okhttp3.mockwebserver.MockResponse
import okhttp3.mockwebserver.MockWebServer
import org.junit.After
import org.junit.AfterClass
import org.junit.BeforeClass


open class BaseMockDataTest {

    var mockWebServer: MockWebServer = MockWebServer()
    private val gson = GsonBuilder().create()
    protected val volumeResponse: VolumesResponse = mockVolumesResponse()

    companion object {
        @BeforeClass @JvmStatic
        fun setUpClass() {
            RxJavaPlugins.setIoSchedulerHandler { Schedulers.trampoline() }
            RxAndroidPlugins.setInitMainThreadSchedulerHandler { Schedulers.trampoline() }
        }
        @AfterClass @JvmStatic
        fun tearDownClass() {
            RxAndroidPlugins.reset()
        }
    }

    protected fun baseUrl(): String {
        return mockWebServer.url("/").toString()
    }

    protected fun setServerResponse(): Unit {
        setServerResponse(200, gson.toJson(volumeResponse).toString())
    }

    protected fun setServerResponse(errorCode: Int, response: String): Unit {
        mockWebServer.enqueue(MockResponse().setResponseCode(errorCode).setBody(response))
    }

    @After
    open fun tearDown() {
        mockWebServer.shutdown();
    }
}


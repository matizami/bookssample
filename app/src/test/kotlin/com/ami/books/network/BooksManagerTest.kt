package com.ami.books.network

import com.ami.books.BaseMockDataTest
import com.ami.books.network.model.Volume
import com.ami.books.volume
import io.reactivex.Observable
import io.reactivex.observers.TestObserver
import org.junit.After
import org.junit.Before
import org.junit.Test

class BooksManagerTest: BaseMockDataTest() {

    private var sut: BooksManager? = null

    @Before
    fun setUp() {
        sut = BooksManager(baseUrl())
    }

    @Test
    fun testBooksRequest() {
        super.setServerResponse()

        //When
        var volObservable : Observable<Array<Volume>> = sut!!.getBooks("test")

        //Then
        TestObserver<Array<Volume>>().apply {
            volObservable.subscribe(this)
            this.assertNoErrors()
            this.assertValueCount(1)
            this.assertValue { values ->
                values.get(0) is Volume
                values.get(0).volumeInfo.title == volume.volumeInfo.title
            }
        }
    }

    @After
    override fun tearDown() {
        super.tearDown()
        sut = null
    }
}
package com.ami.books.ui

import com.ami.books.BaseMockDataTest
import com.ami.books.network.BooksManager
import com.ami.books.network.model.Volume
import com.ami.books.volume
import com.nhaarman.mockito_kotlin.argumentCaptor
import com.nhaarman.mockito_kotlin.mock
import org.junit.After
import org.junit.Before
import org.junit.Test
import org.mockito.Mockito.verify
import kotlin.test.assertEquals
import kotlin.test.assertNotNull
import kotlin.test.assertTrue

class BooksPresenterTest: BaseMockDataTest() {

    val viewMock: BooksView = mock()
    lateinit var presenter: BooksPresenter

    @Before
    fun setUp() {
        presenter = BooksPresenter()
        presenter.setManagerForTesting(BooksManager(baseUrl()))
        presenter.start(viewMock)
    }

    @Test
    fun testStartPresenter() {
        //Given
        setServerResponse()

        //When
        presenter.requestBooks("test query")

        //Then
        argumentCaptor<Array<Volume>>().apply {
            verify(viewMock).showBooks(capture())
            assertNotNull(allValues)
            assertTrue(allValues.size == 1)
            assertEquals(firstValue.get(0).volumeInfo.title, volume.volumeInfo.title)
        }

    }

    @After
    override fun tearDown() {
        super.tearDown()
    }
}

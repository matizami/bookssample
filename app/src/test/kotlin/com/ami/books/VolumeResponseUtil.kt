package com.ami.books

import com.ami.books.network.model.CoverImage
import com.ami.books.network.model.Volume
import com.ami.books.network.model.VolumeInfo
import com.ami.books.network.model.VolumesResponse

val volume: Volume = Volume(VolumeInfo(
        "Test title",
        "Test subtitle",
        arrayOf("Author1", "Author2"),
        "26 June 2017",
        CoverImage("", "")
))

fun mockVolumesResponse(): VolumesResponse {
    return VolumesResponse(arrayOf(volume))
}

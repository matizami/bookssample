package com.ami.books.ui

import android.content.Context
import android.support.v7.widget.RecyclerView
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ImageView
import android.widget.TextView
import com.ami.books.R
import com.ami.books.network.model.Volume
import com.squareup.picasso.Picasso

class BooksAdapter(var context: Context) : RecyclerView.Adapter<BooksAdapter.BookViewHolder>() {

    var books: Array<Volume> = arrayOf<Volume>()

    override fun onBindViewHolder(viewHolder: BookViewHolder?, position: Int) {
        val bookItem = this.books.get(position)
        viewHolder?.titleTextView?.text = bookItem.volumeInfo.title
        if (bookItem.volumeInfo.imageLinks != null)
            Picasso.with(context).load(bookItem.volumeInfo.imageLinks.thumbnail).into(viewHolder?.coverImageView)
    }

    override fun onCreateViewHolder(parent: ViewGroup?, viewType: Int): BookViewHolder {
        val view = LayoutInflater.from(parent?.context).inflate(viewType, parent, false)
        return BookViewHolder(view)
    }

    override fun getItemCount(): Int {
        return this.books.count()
    }

    override fun getItemViewType(position: Int): Int {
        return R.layout.book_item
    }

    fun setItems(books: Array<Volume>): Unit {
        this.books = books
        notifyDataSetChanged()
    }

    class BookViewHolder : RecyclerView.ViewHolder {
        var titleTextView: TextView
        var coverImageView: ImageView

        constructor(itemView: View) : super(itemView) {
            titleTextView = itemView.findViewById(R.id.textViewTitle) as TextView
            coverImageView = itemView.findViewById(R.id.imageViewCover) as ImageView
        }
    }
}

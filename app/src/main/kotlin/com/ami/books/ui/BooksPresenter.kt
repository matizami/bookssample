package com.ami.books.ui

import com.ami.books.network.BooksManager
import io.reactivex.disposables.Disposable

class BooksPresenter {

    private var booksManager: BooksManager = BooksManager()
    private var subscription: Disposable? = null
    private lateinit var view: BooksView
    private var loading = false

    fun start(view: BooksView): Unit {
        this.view = view
        if (loading) {
            requestBooks(view.getQuery())
        }
    }

    fun stop(): Unit {
        subscription?.dispose()
    }

    fun requestBooks(query: String) {
        if (query.length == 0) {
            return
        }
        setLoading(true)
        subscription = booksManager.getBooks(query)
                .subscribe({
                    books ->
                    this.view.showBooks(books)
                    setLoading(false)
                },
                        {
                            error ->
                            this.view.showError(error.localizedMessage)
                            setLoading(false)
                        })
    }

    private fun setLoading(show: Boolean) {
        loading = true
        view.showLoading(show)
    }

    fun setManagerForTesting(booksManager: BooksManager): Unit {
        this.booksManager = booksManager
    }
}
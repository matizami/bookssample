package com.ami.books.ui

import com.ami.books.network.model.Volume

interface BooksView {
    fun showBooks(books: Array<Volume>): Unit
    fun showError(error: String): Unit
    fun showLoading(loading: Boolean)
    fun getQuery(): String
}
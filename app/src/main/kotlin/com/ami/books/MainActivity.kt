package com.ami.books

import android.os.Bundle
import android.support.v7.app.AppCompatActivity
import android.support.v7.widget.LinearLayoutManager
import android.support.v7.widget.RecyclerView
import android.view.View
import com.ami.books.network.model.Volume
import com.ami.books.ui.BooksAdapter
import com.ami.books.ui.BooksPresenter
import com.ami.books.ui.BooksView
import com.jakewharton.rxbinding2.widget.RxTextView
import io.reactivex.android.schedulers.AndroidSchedulers
import kotlinx.android.synthetic.main.activity_main.*
import java.util.concurrent.TimeUnit

class MainActivity : AppCompatActivity(), BooksView {

    override fun showLoading(loading: Boolean) {
        var visibility = if(loading) View.VISIBLE else View.GONE
        progressBar.visibility= visibility
    }

    private lateinit var adapter: BooksAdapter
    private lateinit var layoutManager: RecyclerView.LayoutManager
    private val presenter: BooksPresenter by lazy { BooksPresenter() }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)
        initRecycler()
        initEditText()
    }

    override fun onStart() {
        super.onStart()
        presenter.start(this)
    }

    private fun initRecycler() {
        layoutManager = LinearLayoutManager(this)
        adapter = BooksAdapter(this)
        recyclerViewBooks.layoutManager = layoutManager
        recyclerViewBooks.adapter = adapter
    }

    private fun initEditText() {
        RxTextView.afterTextChangeEvents(editTextSearch)
                .debounce(1000, TimeUnit.MILLISECONDS)
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe { tvChangeEvent ->
                    tvChangeEvent.view().error = null
                    var text = tvChangeEvent.view().text.toString()
                    serchForBooks(text)
                }
    }

    fun serchForBooks(query: String) {
        presenter.requestBooks(query)
    }

    override fun showBooks(books: Array<Volume>): Unit {
        this.adapter.setItems(books)
    }

    override fun showError(error: String) {
        editTextSearch.error = error
    }

    override fun getQuery(): String {
        return editTextSearch.text.toString()
    }

    override fun onStop() {
        presenter.stop()
        super.onStop()
    }
}
package com.ami.books.network

import com.ami.books.network.model.VolumesResponse
import io.reactivex.Observable
import retrofit2.http.GET
import retrofit2.http.Query

interface BooksApiService {

    @GET(Const.VERSION + "/volumes?")
    fun getBooks(@Query("q") searchQuery: String): Observable<VolumesResponse>
}
package com.ami.books.network.model

data class CoverImage (
        var smallThumbnail: String,
        var thumbnail: String
)

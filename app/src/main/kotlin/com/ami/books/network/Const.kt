package com.ami.books.network

object Const {
    const val BASE_URL = "https://www.googleapis.com/books/"
    const val VERSION = "v1"
}


package com.ami.books.network

open abstract class BaseManager(protected val baseUrl: String, private val service: RetrofitService) {

    protected fun getRetrofitService() : RetrofitService {
        return service
    }
}

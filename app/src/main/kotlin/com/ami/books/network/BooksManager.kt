package com.ami.books.network

import com.ami.books.network.model.Volume
import io.reactivex.Observable
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.schedulers.Schedulers

open class BooksManager: BaseManager {

    constructor(): super(Const.BASE_URL, RetrofitService())

    constructor(baseURL: String) : super(baseURL, RetrofitService())

    fun getBooks(searchQuery: String) : Observable<Array<Volume>> {
        return getRetrofitService().executeRequest<BooksApiService>(baseUrl)
                    .getBooks(searchQuery)
                    .subscribeOn(Schedulers.io())
                    .map{ it.items }
                    .observeOn(AndroidSchedulers.mainThread())
    }
}

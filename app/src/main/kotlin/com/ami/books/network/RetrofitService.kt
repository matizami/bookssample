package com.ami.books.network

import okhttp3.OkHttpClient
import okhttp3.logging.HttpLoggingInterceptor
import retrofit2.Retrofit
import retrofit2.adapter.rxjava2.RxJava2CallAdapterFactory
import retrofit2.converter.gson.GsonConverterFactory

class RetrofitService {

    companion object {
        @JvmStatic private val client: OkHttpClient = provideClient()
        @JvmStatic private val rxJavaCallAdapterFactory = RxJava2CallAdapterFactory.create()
        @JvmStatic private val gsonConverterFactory = GsonConverterFactory.create()

        @JvmStatic private fun provideClient(): OkHttpClient {
            val loggInterceptor = HttpLoggingInterceptor()
            loggInterceptor.level = HttpLoggingInterceptor.Level.BODY

            return OkHttpClient.Builder()
                    .addInterceptor(loggInterceptor)
                    .build()
        }
    }

    fun provideRetrofit(baseURL: String): Retrofit {
        return Retrofit.Builder()
                .baseUrl(baseURL)
                .client(client)
                .addCallAdapterFactory(rxJavaCallAdapterFactory)
                .addConverterFactory(gsonConverterFactory)
                .build()
    }

    inline fun <reified ApiServiceClass> executeRequest(baseURL: String): ApiServiceClass {
        return provideRetrofit(baseURL).create(ApiServiceClass::class.java)
    }
}
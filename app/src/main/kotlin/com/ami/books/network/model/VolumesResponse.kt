package com.ami.books.network.model

data class VolumesResponse (
        var items: Array<Volume>
)
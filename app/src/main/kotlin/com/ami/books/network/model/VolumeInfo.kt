package com.ami.books.network.model

data class VolumeInfo(
        var title: String,
        var subtitle: String,
        var authors: Array<String>,
        var publishedDate: String,
        var imageLinks: CoverImage
)